?-
  % Car moving software based on Ackerman principle, which
  % might be used for real time simulation / testing

  G_Zoom_X := 1,
  G_Zoom_Y := 1,
  G_Break := 0,

  % people
  array(x, 20, 0.0),
  array(y, 20, 0.0),
  array(xs, 20, 0.0),
  array(ys, 20, 0.0),
  array(color_p, 20, 0),

  % cars
  G_CAR_HEIGHT := 40,      % car height
  G_CAR_WIDTH := 60,        % car width
  G_TREAD := 0,             % distance between front (back) tires
  G_WHEEL_BASE := 0,        % distance between fron and back tires
  G_TARGET_ANGLE := 0,
  G_RADIUS := 0.0, 


  array(car_x, 5, 200.0),
  array(car_y, 5, 170.0),

  array(car_sp, 5, 0.01),

  array(body_angle, 5, pi / 12),            % body angle
  array(steering_angle, 5, pi / 12),      % steering angle

  array(points, 8, 0.0),
  array(points_new, 8, 0.0),
  array(center, 8, 0.0),
  array(rotation, 4, 0.0),

  array(accel, 5, 0.0),
  array(beta_prim, 5, 0.0),
  array(ackermann_angle, 5, 0.0),
  array(car_color, 5, rgb(0,0,0)),
  init_persons,
  window(title("Robot Car"), size(1130,650)).

win_func(init) :-
    G_Timer := set_timer(_, 0.1, time_func).

win_func(paint) :-
  line(G_Zoom_X * 50, G_Zoom_Y * 50, G_Zoom_X * 1050, G_Zoom_Y * 50, G_Zoom_X * 1050,
       G_Zoom_Y * 550, G_Zoom_X * 50, G_Zoom_Y * 550, G_Zoom_X * 50, G_Zoom_Y * 50),
  X1 := 300 * G_Zoom_X,
  X2 := 780 * G_Zoom_X,
  Y := 300 * G_Zoom_Y,
  R := 100 * G_Zoom_X,
  car_color(0) := rgb(255,0,0),
  car_color(1) := rgb(0,255,0),
  car_color(2) := rgb(0,0,255),
  car_color(3) := rgb(125,0,125),
  car_color(4) := rgb(255,255,255),
  brush(system_color(window)),
 % ellipse(X1 - R, Y - R, X1 + R, Y + R),
 % ellipse(X2 - R, Y - R, X2 + R, Y + R),
  fail.

% win_func(paint) :-
%   pen(0, 0),
%   show_people.

win_func(paint) :-
  pen(3, rgb(255, 0, 0)),
  show_cars.

show_people :-
  for(I, 0, 19),
    brush(color_p(I)),
    ellipse((x(I) - 10) * G_Zoom_X, (y(I) - 10) * G_Zoom_Y,
            (x(I) + 10) * G_Zoom_X, (y(I) + 10) * G_Zoom_Y),
  fail.


show_cars :-
  for(I, 0, 0),
    brush(car_color(I)),

    rotate(car_x(I) - G_CAR_WIDTH / 2, car_y(I) + G_CAR_HEIGHT / 2,
                 car_x(I) + G_CAR_WIDTH / 2, car_y(I) + G_CAR_HEIGHT / 2,
                 car_x(I) - G_CAR_WIDTH / 2, car_y(I) - G_CAR_HEIGHT / 2,
                 car_x(I) + G_CAR_WIDTH / 2, car_y(I) - G_CAR_HEIGHT / 2,
                 car_x(I), car_y(I), body_angle(I)),

    X1 := points(3),
    Y1 := points(7),
    X2 := points(2),
    Y2 := points(6),
    X3 := points(1),
    Y3 := points(5),
    X4 := points(0),
    Y4 := points(4),

    % car body
    fill_polygon(X1, Y1, X2, Y2, X4, Y4, X3, Y3),
    brush(rgb(0,0,0)),

    % used to show car direction
     fill_polygon(X1, Y1, X3, Y3, car_x(I), car_y(I)),
    
   pen(3, rgb(0, 255, 0)),

   % tread
   line(X1, Y1, X3, Y3),
   G_TREAD := sqrt(
             (X1 - X3) ** 2 +
             (Y1 - Y3) ** 2),

   ellipse(car_x(I) - 1,
           car_y(I) - 1,
           car_x(I) + 1,
           car_y(I) + 1),

   pen(3, rgb(0, 255, 0)),

   % wheel base
   line(X1, Y1, X2, Y2), 
   G_WHEEL_BASE := sqrt(
             (X1 - X2) ** 2 +
             (Y1 - Y2) ** 2),
 
    % draw the tires
    TURN_POINT := G_WHEEL_BASE / tg(steering_angle(I)),
    L_ANGLE := arctg(G_WHEEL_BASE / (TURN_POINT - G_TREAD / 2)),
    R_ANGLE := arctg(G_WHEEL_BASE / (TURN_POINT + G_TREAD / 2)),
    ackermann_angle(I) := arctg(G_WHEEL_BASE / TURN_POINT),
    G_RADIUS := G_WHEEL_BASE / ackermann_angle(I),

    % top down right tire
    pen(3, rgb(255, 0, 0)),
    
    rotate(
                 X3 - G_TREAD / 8, Y3 + G_WHEEL_BASE / 8,
                 X3 + G_TREAD / 8, Y3 + G_WHEEL_BASE / 8,
                 X3 - G_TREAD / 8, Y3 - G_WHEEL_BASE / 8,
                 X3 + G_TREAD / 8, Y3 - G_WHEEL_BASE / 8,
                 X3, Y3, R_ANGLE * -1),
    fill_polygon(points(0), points(4), points(1), points(5),
                 points(3), points(7), points(2), points(6)),
  
 
    % top down left tire
       
    rotate(
                 X1 - G_TREAD / 8, Y1 + G_WHEEL_BASE / 8,
                 X1 + G_TREAD / 8, Y1 + G_WHEEL_BASE / 8,
                 X1 - G_TREAD / 8, Y1 - G_WHEEL_BASE / 8,
                 X1 + G_TREAD / 8, Y1 - G_WHEEL_BASE / 8,
                 X1, Y1, L_ANGLE * -1),

    fill_polygon(points(0), points(4), points(1), points(5),
                 points(3), points(7), points(2), points(6)),


    % top front right tire
    fill_polygon(
                 X4 - G_CAR_WIDTH / 8, Y4 + G_CAR_HEIGHT / 8,
                 X4 + G_CAR_WIDTH / 8, Y4 + G_CAR_HEIGHT / 8,
                 X4 + G_CAR_WIDTH / 8, Y4 - G_CAR_HEIGHT / 8,
                 X4 - G_CAR_WIDTH / 8, Y4 - G_CAR_HEIGHT / 8),

    % top front left tire
    fill_polygon(
                 X2 - G_WHEEL_BASE / 8, Y2 + G_TREAD / 8,
                 X2 + G_WHEEL_BASE / 8, Y2 + G_TREAD / 8,
                 X2 + G_WHEEL_BASE / 8, Y2 - G_TREAD / 8,
                 X2 - G_WHEEL_BASE / 8, Y2 - G_TREAD / 8),

  fail.

win_func(size(W, H)) :-
  G_Zoom_X := W / 1130,
  G_Zoom_Y := H / 650,
  update_window(_).

init_persons :-
  for(I, 0, 19),
    x(I) := random(900) + 100,
    y(I) := random(450) + 50,
    change(I),
    color_p(I) := random(256 ** 3),
  fail.

init_persons.

time_func(end) :-
  I := random(20),
  change(I),

  J := random(4) + 1,
  accel(J) := random(10) * 0.1 - 0.5,
  beta_prim(J) := random(3) * 0.1 - 0.1,

 % move_people,
  move_cars,
  main_car,
  update_window(_).

move_people :-
  for(I, 0, 19),
    x(I) := x(I) + xs(I),
    correct_x(I),
    y(I) := y(I) + ys(I),
    correct_y(I),
  fail.

move_people.

main_car :-
  X1 := car_x(0) + cos(body_angle(0)) * car_sp(0)  * G_RADIUS,
  Y1 := car_y(0) + sin(body_angle(0)) * car_sp(0)  * G_RADIUS,
  write(X1 + ", " + Y1 + ", " + car_sp(0)  + ", " + steering_angle(0) + ", " + ackermann_angle(0) +  "\n"),
  (is_out(X1,Y1) -> 
     write("OUT\n"),
    (G_Break = 0 ->
      % car_sp(0) := 0,
      beta_prim(0) := sign(steering_angle(0)) * steering_angle(0) * 0.5,
      accel(0) := car_sp(0) * -1 * 0.1, 
      % G_RADIUS := -1 * G_RADIUS,
      G_Break := 1
    ) 
   else 
      G_Break :=0,
      accel(0) := 0
  ),
  beta_prim(0) := random(3) * 0.1 - 0.1.

main_car.

move_cars :-
  for(I, 0, 0),
     car_sp(I) := car_sp(I) + accel(I),
     increment_steering_angle(beta_prim(I)),
     car_x(I) := car_x(I) + cos(body_angle(I))  * G_RADIUS * car_sp(I),
     car_y(I) := car_y(I) + sin(body_angle(I))  * G_RADIUS * car_sp(I),
     body_angle(I) := body_angle(I) + ackermann_angle(I),
  fail.

move_cars.

increment_steering_angle(VAL) :-
  NEW_ANGLE := 0,
  (steering_angle(0) + VAL > pi ->
     NEW_ANGLE := VAL - steering_angle(0) 
  ),
  (steering_angle(0) + VAL =< pi ->
     NEW_ANGLE := steering_angle(0) + VAL
  ),
  (NEW_ANGLE < 0.4,
    NEW_ANGLE > 0.4 * -1 ->
     steering_angle(0) := NEW_ANGLE
  ).

is_out(X, Y) :-
   (X < 60;  X >1040); (Y < 60; Y > 540).

correct_x(I) :-
  (x(I) =< 60 ->
    xs(I) := -1 * xs(I),
    x(I) := x(I) + 2 * xs(I)
  ),
  (x(I) >= 1040 ->
    xs(I) := -1 * xs(I),
    x(I) := x(I) + 2 * xs(I)
  ).

correct_y(I) :-
  (y(I) =< 60 ->
    ys(I) := -1 * ys(I),
    y(I) := y(I) + 2 * ys(I)
  ),
  (y(I) >= 540 ->
    ys(I) := -1 * ys(I),
    y(I) := y(I) + 2 * ys(I)
  ).


change(I) :-
  Speed := random(10),
  Alpha := random(2 * pi) + 1,
  xs(I) := cos(Alpha) * Speed,
  ys(I) := sin(Alpha) * Speed.

rotate(X1, Y1, X2, Y2, X3, Y3, X4, Y4, A, B, ROTATION_ANGLE) :-
    points(0) := X1,
    points(1) := X2,
    points(2) := X3,
    points(3) := X4,
    points(4) := Y1,
    points(5) := Y2,
    points(6) := Y3,
    points(7) := Y4,

   center(0) := A,
   center(1) := A,
   center(2) := A,
   center(3) := A,
   center(4) := B,
   center(5) := B,
   center(6) := B,
   center(7) := B,

   rotation(0) := cos(ROTATION_ANGLE),
   (abs(rotation(0)) < 0.0001 ->
         rotation(0) := 0
   ),
   rotation(1) := sin(ROTATION_ANGLE) * -1,
  (abs(rotation(1)) < 0.0001 ->
      rotation(1) := 0
  ),
   rotation(2) := sin(ROTATION_ANGLE),
   (abs(rotation(2)) < 0.0001 ->
      rotation(2) := 0
    ),
   rotation(3) := cos(ROTATION_ANGLE),
  (abs(rotation(3)) < 0.0001 ->
     rotation(3) := 0
  ),

  substract_matrices,
  multiple_matrices,
  add_matrices.
  %fill_polygon(points(0), points(4), points(1), points(5), points(2), points(6), points(3), points(7)).

add_matrices :-
  for(I, 0, 8),
    points(I) := points(I) + center(I),
  fail.

add_matrices.

substract_matrices :-
    for(I, 0, 8),
      points(I) := points(I) - center(I),
    fail.

substract_matrices.

multiple_matrices :-
  points_new(0) := rotation(0) * points(0) + rotation(1) * points(4),
  points_new(1) := rotation(0) * points(1) + rotation(1) * points(5),
  points_new(2) := rotation(0) * points(2) + rotation(1) * points(6),
  points_new(3) := rotation(0) * points(3) + rotation(1) * points(7),
  points_new(4) := rotation(2) * points(0) + rotation(3) * points(4),
  points_new(5) := rotation(2) * points(1) + rotation(3) * points(5),
  points_new(6) := rotation(2) * points(2) + rotation(3) * points(6),
  points_new(7) := rotation(2) * points(3) + rotation(3) * points(7),

  points(0) := points_new(0),
  points(1) := points_new(1),
  points(2) := points_new(2), 
  points(3) := points_new(3),
  points(4) := points_new(4),
  points(5) := points_new(5),
  points(6) := points_new(6),
  points(7) := points_new(7).
























































































































































































































































































































































































































































































































































































































